<?php

namespace GKZF2\Webservice;

use GKZF2\Core\Error\ErrorManager;
use GKZF2\Core\ExchangeData\ExchangeData;
use GKZF2\Core\ExchangeData\ExchangeDataGetterInterface;
use GKZF2\Webservice\Result\Result;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\ServiceManager\ServiceManager;

abstract class AbstractRestController extends AbstractRestfulController {
    /**
     *
     * @var ErrorManager
     */
    private $errorManager;

    /**
     *
     * @return ErrorManager
     */
    private function getErrorManager() {
        if (null === $this->errorManager) {
            $this->errorManager = $this->getServiceLocator()->get('GKZF2\Webservice\Error\ErrorManager');
        }
        return $this->errorManager;
    }
    
    /**
     * 
     * @return \Zend\ServiceManager\ServiceLocatorInterface|ServiceManager
     */
    protected function getServiceManager() {
        return $this->getServiceLocator();
    }

    /**
     * 
     * @param int $errorCode
     * @param array $extraMessages
     * @return mixed
     */
    protected function generateErrorArray($errorCode, $extraMessages = array()) {
        $error = $this->getErrorManager()->generateAndLogError($errorCode, $extraMessages);
        $exchangeData = new ExchangeData();
        $exchangeData->setData($error);
        return Result::create($errorCode, $exchangeData);
    }

    /**
     *
     * @param string $value
     * @return null
     */
    protected function generateLogInFile($value) {
        return $this->getErrorManager()->generateLogInFile($value);
    }

    /**
     *
     * @param ExchangeDataGetterInterface $exchangeDataGetterInterface
     * @param int $code
     * @return array
     */
    protected function returnValidRequestFromOnlyOneExchangeData(
            ExchangeDataGetterInterface $exchangeDataGetterInterface, $code = 200) {
        return Result::create($code, $exchangeDataGetterInterface->getExchangeData());
    }

    /**
     *
     * @param ExchangeDataGetterInterface[] $exchangeDataGetterInterfaces
     * @param int $code
     * @return Result
     */
    protected function returnValidRequestFromExchangeDataArray($exchangeDataGetterInterfaces = array(), $code = 200) {
        return Result::create($code, ExchangeData::exchangeDataGetterInterfaceListToExchangeData($exchangeDataGetterInterfaces));
    }

    /**
     *
     * @param array $arrayOfArray
     * @param int $code
     * @return mixed
     */
    protected function returnValidRequestFromArrayOfArray($arrayOfArray = array(), $code = 200) {
        if ($arrayOfArray && !empty($arrayOfArray)) {
//            $data = array('error' => 0);
            $data = array();
            foreach ($arrayOfArray as $parameterName => $parameterValues) {
                $data[$parameterName] = $parameterValues;
            }
            $exchangeData = new ExchangeData();
            $exchangeData->setData($data);

            return Result::create($code, $exchangeData);
        }
        else {
            return $this->generateErrorArray(501, 'Model can not be empty');
        }
    }

    protected function ok(ExchangeData $exchangeData, $code = 200) {
        return Result::create($code, $exchangeData);
    }

    public function getList() {
        return $this->generateErrorArray(404);
    }

    public function get($id) {
        return $this->generateErrorArray(404);
    }

    public function create($data) {
        return $this->generateErrorArray(404);
    }

    public function delete($id) {
        return $this->generateErrorArray(404);
    }

    public function update($id, $data) {
        return $this->generateErrorArray(404);
    }
}
