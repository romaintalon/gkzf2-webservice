<?php
namespace GKZF2\Webservice;

use Zend\ServiceManager\ServiceManager;
use Zend\ServiceManager\ServiceManagerAwareInterface;

abstract class AbstractWebservice implements ServiceManagerAwareInterface {

    /**
     *
     * @var ServiceManager
     */
    private $serviceManager;

    /**
     *
     * @param ServiceManager $serviceManager
     */
    public function __construct(ServiceManager $serviceManager) {
        $this->serviceManager = $serviceManager;
    }

    /**
     *
     * @return ServiceManager
     */
    public function getServiceManager() {
        return $this->serviceManager;
    }

    public function setServiceManager(ServiceManager $serviceManager) {
        $this->serviceManager = $serviceManager;
    }

    public function getConfig() {
        return $this->getServiceManager()->get('Config');
    }
}
