<?php

namespace GKZF2\Webservice\Result;

use GKZF2\Core\ExchangeData\ExchangeData;

class Result {
    /** @var  integer code */
    protected $code;
    /** @var  ExchangeData $exchangeData */
    protected $exchangeData;

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->exchangeData->toArray();
    }

    /**
     * @param integer $code
     * @param ExchangeData $exchangeData
     * @return Result
     */
    public static function create($code, $exchangeData) {
        $result = new self();
        $result->code = $code;
        $result->exchangeData = $exchangeData;
        return $result;
    }

}