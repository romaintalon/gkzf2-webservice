<?php

namespace GKZF2\Webservice;

class AbstractAuthenticatedWebservice extends AbstractWebservice {
    /**
     *
     * @return \Zend\Authentication\AuthenticationService
     */
    protected function getAuthentication() {
        return $this->getServiceManager()->get('GKZF2\Authentication');
    }
}