<?php

namespace GKZF2\Webservice\Error;

use GKZF2\Core\Processor\AbstractPostProcessor;
use GKZF2\Core\Processor\AbstractProcessor;
use Zend\Mvc\Application;
use Zend\Mvc\MvcEvent;
use Zend\Http\PhpEnvironment\Response;

use GKZF2\Webservice\Exception\GenericException;

class ErrorProcessor extends AbstractProcessor {

    /**
     * @param null|\Exception $exception
     * @return array
     */
    private function getErrorVarsAndGenerateLogInFile($exception) {
        
        $vars = array();
        
        if (isset($exception)) {
            
            if ($this->getConfiguration()['errors']['show_exceptions']['message']) {
                $vars['error-message'] = $exception->getMessage();
            }
            
            if ($this->getConfiguration()['errors']['show_exceptions']['trace']) {
                $vars['error-trace'] = $exception->getTrace();
            }
        }
        
        $apiErrorProcessor = $this->getServiceManager()->get('GKZF2\Core\Error\ErrorManager');
        
        if (empty($vars)) { // We have no more iformation in the generated log so we generate a default answer
            $extraError = array('message' => 'Something went wrong in ErrorProcessor.processAndGetResponse() ... (no $vars given)');
            $vars = $apiErrorProcessor->generateAndLogError(505, $extraError, 'err');
        }
        else { // We have an answer so we store it in our default log file
            $apiErrorProcessor->generateLogInFile($exception->getTraceAsString(), 'err');
        }
        
        return $vars;
    }
    
    private function setSpecificStatusCode($statusCode) {
        // wwhat returns $this->getMvcEvent()->getResponse() ?
        $this->getMvcEvent()->getResponse()->setStatusCode($statusCode);
        return $statusCode;
    }
    
    private function generateStatusCode($errorParam) {
                
        if ($errorParam === Application::ERROR_CONTROLLER_NOT_FOUND
                || $errorParam === Application::ERROR_ROUTER_NO_MATCH) {
            
            return $this->setSpecificStatusCode(Response::STATUS_CODE_501);
        }
        else {
            return $this->setSpecificStatusCode(Response::STATUS_CODE_500);
        }
    }
    
    public function processAndGetResponse(MvcEvent $e) {
        $this->setProperties($e);

        array_key_exists('exception', $this->getEventParams()) ?
            $exception = $this->getEventParams()['exception'] :
            $exception = null;

        $vars['error'] = $this->getErrorVarsAndGenerateLogInFile($exception);
        
        if ($exception instanceof GenericException) {
            
            if (!$exception->getShow_error_trace())
                unset($vars['error']['error-trace']);
            
            $statusCode = $exception->getOutput_http_status_code();
        }
        else {
            
            $statusCode = $this->generateStatusCode($this->getEventParams()['error']);
        }

        /** @var AbstractPostProcessor $postProcessor */
        $postProcessor = $this->getServiceManager()->get('di')
                ->get($this->getConfiguration()['errors']['post_processor'], array(
                    'response' => $this->getMvcEvent()->getResponse(),
                    'statusCode' => $statusCode,
                    'data' => $vars
                ));

        $this->getMvcEvent()->stopPropagation();

        return $postProcessor->processAndGetResponse();
    }
}
