<?php

namespace GKZF2\Webservice\Error;

use Zend\Log\Logger;
use Zend\Log\Writer\Stream;
use Zend\ServiceManager\ServiceManager;

class ErrorManager extends \GKZF2\Core\Error\ErrorManager
{
    const ERROR_SERVER = '501';
    const ERROR_NO_CONTENT = '204';
    const ERROR_NOT_FOUND = '404';
    const ERROR_NOT_ENOUGH_RIGHTS = '403';
    const ERROR_CAN_NOT_BE_PROCESSED = '409';
    const ERROR_BAD_HTTP_VERSION = '505';
}