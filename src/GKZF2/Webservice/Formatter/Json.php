<?php

namespace GKZF2\Webservice\Formatter;

use GKZF2\Core\Processor\AbstractPostProcessor;

class Json extends AbstractPostProcessor {

    public function processAndGetResponse() {
        $data = json_encode($this->_data, JSON_NUMERIC_CHECK);
        $this->_response->setStatusCode($this->_statusCode)->setContent($data);
        
        $headers = $this->_response->getHeaders();
        $headers->addHeaderLine('Content-Type', 'application/json; charset=utf-8');
        $this->_response->setHeaders($headers);

        return $this->_response;
    }
}
