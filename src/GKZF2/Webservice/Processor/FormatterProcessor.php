<?php
namespace GKZF2\Webservice\Processor;

use GKZF2\Core\Processor\AbstractPostProcessor;
use GKZF2\Core\Processor\AbstractProcessor;
use GKZF2\Webservice\Result\Result;
use Zend\Mvc\MvcEvent;

class FormatterProcessor extends AbstractProcessor {
    
    public function processAndGetResponse(MvcEvent $e) {
        $routeMatch = $e->getRouteMatch();
        $formatter = $routeMatch->getParam('formatter', false);

        if ($formatter !== false) {

            /** @var Result $result */
            $result = $e->getResult();

            /** @var \Zend\Di\Di $di */
            $di = $e->getTarget()->getServiceLocator()->get('di');

            /** @var AbstractPostProcessor $postProcessor */
            $postProcessor = $di->get($formatter, array( //$formatter has to match with a key defined in module.config.php
                'response' => $e->getResponse(),
                'statusCode' => $result->getCode(),
                'data' => $result->getData()
            ));
            
            return $postProcessor->processAndGetResponse();
        }

        return null;
    }
}