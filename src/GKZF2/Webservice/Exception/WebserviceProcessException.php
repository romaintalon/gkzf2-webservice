<?php

namespace GKZF2\Webservice\Exception;

class WebserviceProcessException extends GenericException {

    public function __construct($message = null, $code = 0, $previous = null) {

        $exception_message = isset($message) ? "Service process exception: " . $message : "Service process exception: no details.";
        parent::__construct($exception_message, $code, $previous, 500, TRUE);
    }
}