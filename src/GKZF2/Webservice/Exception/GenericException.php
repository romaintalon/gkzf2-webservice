<?php

namespace GKZF2\Webservice\Exception;

class GenericException extends \Exception {
    
    private $extra_message;
    private $output_http_status_code;
    private $show_error_trace;
    
    public function __construct($exception_message = null, $exception_code = 0, $exception_previous = null,
            $output_http_status_code = 500, $show_error_trace = TRUE) {
        
        $this->extra_message = isset($exception_message)? $exception_message: 'Generic exception: no message.';
        $this->output_http_status_code = $output_http_status_code;
        $this->show_error_trace = $show_error_trace;
        
        parent::__construct($this->extra_message, $exception_code, $exception_previous);
    }
    
    function getExtra_message() {
        return $this->extra_message;
    }

    function getOutput_http_status_code() {
        return $this->output_http_status_code;
    }

    function getShow_error_trace() {
        return $this->show_error_trace;
    }
}