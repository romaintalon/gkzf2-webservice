<?php

namespace GKZF2\Webservice\ServiceLoader;

use GKZF2\Core\Exception\ServiceLoaderNotInitializedException;
use GKZF2\Core\ServiceConfig\ServiceConfig;
use GKZF2\Core\ServiceLoader\ServiceLoaderInterface;
use GKZF2\Webservice\ServiceLoader\Generator\WebserviceGenerator;
use GKZF2\Webservice\ServiceLoader\GeneratorClass\WebserviceGeneratorClass;

abstract class ServiceLoader implements ServiceLoaderInterface {
    protected $_webservices = null;

    /**
     * Must set the $_models value to an array
     * @param WebserviceGeneratorClass[] $webservices
     * @return $this
     */
    abstract public function setWebservices(array $webservices);

    /**
     * @param ServiceConfig $config
     * @return mixed|void
     * @throws ServiceLoaderNotInitializedException
     */
    public function loadServices(ServiceConfig $config) {
        if ($this->_webservices === null || !is_array($this->_webservices)) {
            throw new ServiceLoaderNotInitializedException();
        }

        $serviceGenerator = new WebserviceGenerator();
        foreach ($this->_webservices as $webservice) {
            $serviceGenerator->generate($config, $webservice);
        }
    }
}