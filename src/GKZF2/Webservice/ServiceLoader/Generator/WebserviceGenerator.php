<?php

namespace GKZF2\Webservice\ServiceLoader\Generator;

use GKZF2\Core\ServiceConfig\Generator\AbstractGenerator;
use GKZF2\Core\ServiceConfig\GeneratorClass\AbstractGeneratorClass;
use GKZF2\Core\ServiceConfig\ServiceConfig;
use GKZF2\Webservice\AbstractWebservice;
use GKZF2\Webservice\ServiceLoader\GeneratorClass\WebserviceGeneratorClass;
use Zend\ServiceManager\ServiceManager;

class WebserviceGenerator extends AbstractGenerator {

    /**
     * @param ServiceConfig $config
     * @param AbstractGeneratorClass|WebserviceGeneratorClass $generatorClass
     * @return $this
     */
    public function generate(ServiceConfig $config, AbstractGeneratorClass $generatorClass) {
        // generate Service
        $config->addService(
            ServiceConfig::TYPE_FACTORIES, $generatorClass->getServiceName(),
            function(ServiceManager $sm) use ($generatorClass) {
                $serviceClass = sprintf('%s\Webservice\%sService', $generatorClass->module, $generatorClass->name);
                /** @var AbstractWebservice $service */
                $service = new $serviceClass($sm);
                return $service;
            }
        );

        return $this;
    }

}
