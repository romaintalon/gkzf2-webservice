<?php

namespace GKZF2\Webservice\ServiceLoader\GeneratorClass;

use GKZF2\Core\ServiceConfig\GeneratorClass\AbstractGeneratorClass;

class WebserviceGeneratorClass extends AbstractGeneratorClass {
    public $module;
    public $package;
    public $name;

    /**
     * @param string $module
     * @param string $name
     * @return WebserviceGeneratorClass
     */
    public static function create($module, $name) {
        $modelGeneratorClass = new self();
        $modelGeneratorClass->module = $module;
        $modelGeneratorClass->name = $name;

        return $modelGeneratorClass;
    }


    /**
     * @return string
     */
    public function getServiceName()
    {
        return sprintf('%s\Webservice\%sService', $this->module, $this->name);
    }
}