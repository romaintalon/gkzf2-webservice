<?php

return array(
    'router' => array(
        'routes' => array(
            'restful' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/API/:controller[.:formatter][/:id]',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'formatter' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'formatter' => 'json',
                    ),
                ),
            ),
        ),
    ),
);