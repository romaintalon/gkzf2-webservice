<?php

use GKZF2\Webservice\Error\ErrorManager;

return array(
    'errors' => array(
        'post_processor' => 'json',
        'core_error_types' => array(
            ErrorManager::ERROR_SERVER => array(
                'code' => 501,
                'data' => array(
                    'error' => 501,
                    'message' => 'Error server: please check the logs.'
                ),
            ),
            ErrorManager::ERROR_NO_CONTENT => array(
                'code' => 204,
                'data' => array(
                    'error' => 204,
                    'message' => 'No content.'
                ),
            ),
            ErrorManager::ERROR_NOT_FOUND => array(
                'code' => 404,
                'data' => array(
                    'error' => 404,
                    'message' => 'Not Found.'
                ),
            ),
            ErrorManager::ERROR_CAN_NOT_BE_PROCESSED => array(
                'code' => 409,
                'data' => array(
                    'error' => 409,
                    'message' => 'Can not be processed.'
                ),
            ),
            ErrorManager::ERROR_NOT_ENOUGH_RIGHTS => array(
                'code' => 403,
                'data' => array(
                    'error' => 403,
                    'message' => 'Not enough rights.'
                )
            ),
            ErrorManager::ERROR_BAD_HTTP_VERSION => array(
                'code' => 505,
                'data' => array(
                    'error' => 505,
                    'message' => 'Server error'
                )
            )
        )
    )
);

