<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace GKZF2\Webservice;

use GKZF2\Core\ServiceConfig\ServiceConfig;
use GKZF2\Webservice\Error\ErrorManager;
use GKZF2\Webservice\Error\ErrorProcessor;
use GKZF2\Webservice\Processor\FormatterProcessor;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\ServiceManager\ServiceManager;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        /** @var \Zend\EventManager\SharedEventManager $sharedEvents */
        $sharedEvents = $e->getApplication()->getEventManager()->getSharedManager();

        // Priority = 0 < 1 = Zend\Mvc\DispatchListener priority
        $sharedEvents->attach('Zend\Mvc\Controller\AbstractRestfulController', MvcEvent::EVENT_DISPATCH, array(new FormatterProcessor(), 'processAndGetResponse'), -100);

        if ($e->getResponse() instanceof \Zend\Http\Response) {
            $sharedEvents->attach('Zend\Mvc\Application', MvcEvent::EVENT_DISPATCH_ERROR, array(new ErrorProcessor, 'processAndGetResponse'), 999);
        }
    }

    public function getConfig()
    {
        $moduleConfig = include __DIR__ . '/config/module.config.php';
        $routerConfig = include __DIR__ . '/config/router.config.php';
        $errorsConfig = include __DIR__ . '/config/errors.config.php';

        return array_merge($moduleConfig, $routerConfig, $errorsConfig);
    }

    public function getAutoloaderConfig()
    {

        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/', __NAMESPACE__),
                ),
            ),
        );
    }

    public function getServiceConfig() {

        $config = new ServiceConfig();
        $config->addService(ServiceConfig::TYPE_FACTORIES, 'GKZF2\Webservice\Error\ErrorManager', function($sm) {
            /** @var ServiceManager $sm */
            $config = $sm->get('Configuration');
            return new ErrorManager($sm,
                $config['errors']['core_error_types'],
                $config['errors']['core_show_errors'],
                $config['errors']['core_error_file_destination'],
                $config['errors']['core_error_log_format']
            );
        });

        return $config->getResult();
    }
}
